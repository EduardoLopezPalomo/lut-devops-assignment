# specify a suitable source image
FROM node:14

WORKDIR /app

COPY package*.json ./

RUN npm ci

# copy the application source code files
COPY ./  /app

EXPOSE 3000

# specify the command which runs the application
CMD ["node", "app.js"]